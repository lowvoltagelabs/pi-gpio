These files are released under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
http://creativecommons.org/licenses/by-sa/3.0/

Released by Low Voltage Labs
http://lowvoltagelabs.com/
