Pi GPIO reference board
=======================

KiCAD source files for the Low Voltage Labs [Pi GPIO reference card](http://lowvoltagelabs.com/products/pi-gpio/)

The missing Raspberry Pi GPIO pinout

Features:

* Only 0.8mm thick.
* Compatible with Raspberry Pi Rev 1 and Rev 2 GPIO.
* Idiot proof, really! You can’t put it on backwards.
* Also makes a great keychain.

License Information
-------------------

The hardware is released under [Creative Commons Share-alike 3.0](http://creativecommons.org/licenses/by-sa/3.0/).

Author
------

Eric Thompson, Low Voltage Labs

